import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class AddIsExcludedFromBalance1710159324486 implements MigrationInterface {
    public async up(q: QueryRunner): Promise<void> {
        await q.addColumn(
            'category',
            new TableColumn({
                name: 'excludeFromBalance',
                type: 'boolean',
                isNullable: false,
                default: false,
            })
        );
    }

    public async down(): Promise<void> {
        // Empty
    }
}
